﻿using System;

namespace DZ2_task2
{
    public enum TypesOfBankAccounts
    {
        current,
        credit,
        deposit,
        budget,
        intendedForSocialPayments
    }

    public struct InformationOfBankAccounts
    {
        public int Number;
        public TypesOfBankAccounts Type;
        public decimal Balance;
        public InformationOfBankAccounts(int number, TypesOfBankAccounts type, decimal balance)
        {
            Number = number;
            Type = type;
            Balance = balance;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            InformationOfBankAccounts account = new InformationOfBankAccounts(12345, TypesOfBankAccounts.budget, 5000000);
            Console.WriteLine("Number is {0}\nType is {1}\nBalance is {2}", account.Number, account.Type, account.Balance);
        }
    }
}
