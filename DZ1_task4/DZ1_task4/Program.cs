﻿using System;

namespace DZ1_task4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a, b and c coefficients separated by a space:");
            string[] s = Console.ReadLine().Split(' ');
            int a = 0, b = 0, c = 0;
            bool success = s.Length == 3 &&
                int.TryParse(s[0], out a) && a != 0 &&
                int.TryParse(s[1], out b) &&
                int.TryParse(s[2], out c);
            if (success && b != 0 && c != 0)
            {
                var D = b * b - 4 * a * c;
                if (D < 0)
                {
                    Console.WriteLine("No roots of this eqiation");
                }
                if (D == 0)
                {
                    Console.WriteLine(b * (-1) / (2 * a));
                }
                if (D > 0)
                {
                    Console.WriteLine(((-b) + Math.Sqrt(D)) / (2 * a));
                    Console.WriteLine(((-b) - Math.Sqrt(D)) / (2 * a));
                }
            }
            else if (success && b == 0 && c != 0 && (-c / a) >= 0)
            {
                Console.WriteLine(Math.Sqrt(-c / a));
                Console.WriteLine(Math.Sqrt(-c / a) * (-1));
            }
            else if (success && b != 0 && c == 0)
            {
                Console.WriteLine(0);
                Console.WriteLine(-b / a);
            }
            else
            {
                Console.WriteLine("There is no roots of the equation or the data is incorrect");
            }
        }
    }
}
