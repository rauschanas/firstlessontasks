﻿using System;

namespace DZ2_task1
{
    enum TypesOfBankAccounts
    {
        current,
        credit,
        deposit,
        budget,
        intendedForSocialPayments
    }
    class Program
    {
        static void Main(string[] args)
        {
            var typesOfBankAccounts = TypesOfBankAccounts.credit;
            Console.WriteLine(typesOfBankAccounts);
        }
    }
}
