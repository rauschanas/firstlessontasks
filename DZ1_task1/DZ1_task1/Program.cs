﻿using System;

namespace DZ1_task1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is you name?\n");
            string name = Console.ReadLine();
            Console.WriteLine("Hello {0}!", name);
        }
    }
}
