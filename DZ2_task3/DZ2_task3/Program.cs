﻿using System;

namespace DZ2_task3
{
    public enum HEI
    {
        Oxford,
        MIT,
        Standford,
        Caltech,
        MSU
    }

    public struct Employee
    {
        public string FirstName;
        public string LastName;
        public decimal Salary;
        public HEI HEI;

        public Employee(string firstName, string lastName, decimal salary, HEI hei)
        {
            FirstName = firstName;
            LastName = lastName;
            Salary = salary;
            HEI = hei;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Employee employee = new Employee("Ivan", "Ivanoff", 800000, HEI.Caltech);
            Console.WriteLine("First name of employee is {0}\nLast name of employee is {1}\nSalary of employee is {2}\nHEI of employee is {3}",
                employee.FirstName, employee.LastName, employee.Salary, employee.HEI);
        }
    }
}
