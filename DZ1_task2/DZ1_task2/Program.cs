﻿using System;
using System.Collections.Generic;

namespace DZ1_task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter two numbers separated by a space:");
            string[] numbers = Console.ReadLine().Split();
            if (numbers.Length == 2 && int.TryParse(numbers[0], out int a) &&
                int.TryParse(numbers[1], out int b) && b != 0)
            {
                Console.WriteLine(a / b);
            }
            else
            {
                Console.WriteLine("Data is incorrect");
            }
        }
    }
}
