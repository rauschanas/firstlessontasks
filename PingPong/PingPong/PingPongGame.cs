﻿using System;

namespace PingPong
{
    public class PingPongGame
    {
        private const double BotDecisionAccuracy = 0.9;

        private readonly PingPongGameField _gameField;

        private int _leftScore;
        private int _rightScore;

        public PingPongGame(PingPongGameField gameField, bool isLeftBot = false, bool isRightBot = false)
        {
            _gameField = gameField;
            _leftScore = 0;
            _rightScore = 0;
            IsPaused = true;
            IsLeftBot = isLeftBot;
            IsRightBot = isRightBot;
            LeftRacketMoveUp = false;
            RightRacketMoveUp = false;
            LeftRacketMoveDown = false;
            RightRacketMoveDown = false;
        }

        public bool IsPaused { get; set; }

        public bool IsLeftBot { get; set; }
        public bool IsRightBot { get; set; }

        public bool LeftRacketMoveUp { get; set; }
        public bool RightRacketMoveUp { get; set; }
        public bool LeftRacketMoveDown { get; set; }
        public bool RightRacketMoveDown { get; set; }


        private event ScoreChangedEventHandler _scoreChanged;

        public event ScoreChangedEventHandler ScoreChanged
        {
            add => _scoreChanged += value;
            remove => _scoreChanged -= value;
        }

        public void HandleGameTick(object sender, EventArgs e)
        {
            if (!IsPaused)
            {
                if (IsLeftBot)
                {
                    _gameField.MoveLeftRacketAsBot(BotDecisionAccuracy);
                }
                else
                {
                    if (LeftRacketMoveUp && !LeftRacketMoveDown)
                    {
                        _gameField.MoveLeftRacketUp();
                    }
                    if (LeftRacketMoveDown && !LeftRacketMoveUp)
                    {
                        _gameField.MoveLeftRacketDown();
                    }
                }
                if (IsRightBot)
                {
                    _gameField.MoveRightRacketAsBot(BotDecisionAccuracy);
                }
                else
                {
                    if (RightRacketMoveUp && !RightRacketMoveDown)
                    {
                        _gameField.MoveRightRacketUp();
                    }
                    if (RightRacketMoveDown && !RightRacketMoveUp)
                    {
                        _gameField.MoveRightRacketDown();
                    }
                }

                if (_gameField.MoveBall(ref _leftScore, ref _rightScore))
                {
                    _scoreChanged?.Invoke(this, new ScoreChangedEventArgs(_leftScore, _rightScore));
                    _gameField.InitElementsPosition();
                    IsPaused = true;
                }
            }
        }
    }
}
