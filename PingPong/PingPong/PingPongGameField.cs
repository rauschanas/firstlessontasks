﻿using System;
using System.Windows;
using System.Windows.Shapes;

namespace PingPong
{
    public class PingPongGameField
    {
        private const int BallVelocity = 10;
        private const int RacketShift = 7;

        private static readonly Random Random = new Random();

        private readonly Thickness _initialLeftRacketMargin;
        private readonly Thickness _initialRightRacketMargin;
        private readonly Thickness _initialBallMargin;

        private readonly Rectangle _leftRacket;
        private readonly Rectangle _rightRacket;
        private readonly Ellipse _ball;
        private readonly int _border;
        private readonly int _width;
        private readonly int _height;

        private int _ballDirectionX;
        private int _ballDirectionY;

        public PingPongGameField(Rectangle leftRacket, Rectangle rightRacket,
            Ellipse ball, int width, int height, int border)
        {
            _initialLeftRacketMargin = leftRacket.Margin;
            _initialRightRacketMargin = rightRacket.Margin;
            _initialBallMargin = ball.Margin;

            _leftRacket = leftRacket;
            _rightRacket = rightRacket;
            _ball = ball;
            _width = width;
            _height = height;
            _border = border;

            InitBallDirection();
        }

        public void InitElementsPosition()
        {
            _leftRacket.Margin = _initialLeftRacketMargin;
            _rightRacket.Margin = _initialRightRacketMargin;
            _ball.Margin = _initialBallMargin;
            InitBallDirection();
        }

        public bool MoveBall(ref int leftScore, ref int rightScore)
        {
            double length = Math.Sqrt(Math.Pow(_ballDirectionX, 2) + Math.Pow(_ballDirectionY, 2));
            double x = _ballDirectionX / length * BallVelocity;
            double y = _ballDirectionY / length * BallVelocity;

            Thickness virtualMargin = new Thickness(_ball.Margin.Left + x,
                _ball.Margin.Top - y, _ball.Margin.Right - x, _ball.Margin.Bottom + y);

            if (virtualMargin.Top < 2 * _border - _height / 2 || virtualMargin.Top > _height / 2 - 2 * _border)
            {
                virtualMargin.Top += 2 * y;
                virtualMargin.Bottom -= 2 * y;
                _ballDirectionY = -_ballDirectionY;
            }
            else if (_ballDirectionX < 0 && virtualMargin.Left > -(_leftRacket.Margin.Right - BallVelocity) / 2 &&
                virtualMargin.Left < _leftRacket.Width - _leftRacket.Margin.Right / 2 &&
                virtualMargin.Top < _leftRacket.Margin.Top + (_leftRacket.Height + BallVelocity) / 2 &&
                virtualMargin.Top > _leftRacket.Margin.Top - (_leftRacket.Height + BallVelocity) / 2 ||
                _ballDirectionX > 0 && virtualMargin.Left < (_rightRacket.Margin.Left + BallVelocity) / 2 &&
                virtualMargin.Left > _rightRacket.Margin.Left / 2 - _rightRacket.Width &&
                virtualMargin.Top < _rightRacket.Margin.Top + (_rightRacket.Height + BallVelocity) / 2 &&
                virtualMargin.Top > _rightRacket.Margin.Top - (_rightRacket.Height + BallVelocity) / 2)
            {
                Rectangle collisionRacket = _ballDirectionX < 0 ? _leftRacket : _rightRacket;
                double distanceToMiddle = virtualMargin.Top - collisionRacket.Margin.Top;

                if (Math.Abs(distanceToMiddle) * 2 > collisionRacket.Width)
                {
                    int ballDirectionYSign = Math.Sign(_ballDirectionY);
                    int distanceSign = Math.Sign(distanceToMiddle);

                    double ratio = (distanceToMiddle / collisionRacket.Height * 2 + distanceSign) * 2;

                    int dy = Convert.ToInt32(Math.Abs(ballDirectionYSign == distanceSign ?
                        _ballDirectionY / ratio : _ballDirectionY * ratio));

                    if (dy > 0)
                    {
                        _ballDirectionY = dy * ballDirectionYSign;
                    }
                }

                virtualMargin.Left -= 2 * x;
                virtualMargin.Right += 2 * x;
                _ballDirectionX = -_ballDirectionX;
            }
            else if (virtualMargin.Left < -_width / 2 || virtualMargin.Left > _width / 2)
            {
                if (virtualMargin.Left < 0)
                {
                    rightScore++;
                }
                else
                {
                    leftScore++;
                }

                return true;
            }

            _ball.Margin = virtualMargin;
            return false;
        }

        public void MoveLeftRacketDown()
        {
            if (_leftRacket.Margin.Top > (_leftRacket.Height - _height) / 2 - RacketShift + _border &&
                _leftRacket.Margin.Top < (_height - _leftRacket.Height) / 2 - RacketShift - _border)
            {
                _leftRacket.Margin = new Thickness(_leftRacket.Margin.Left, _leftRacket.Margin.Top + RacketShift,
                    _leftRacket.Margin.Right, _leftRacket.Margin.Bottom - RacketShift);
            }
        }

        public void MoveLeftRacketUp()
        {
            if (_leftRacket.Margin.Top > (_leftRacket.Height - _height) / 2 + RacketShift + _border &&
                _leftRacket.Margin.Top < (_height - _leftRacket.Height) / 2 + RacketShift - _border)
            {
                _leftRacket.Margin = new Thickness(_leftRacket.Margin.Left, _leftRacket.Margin.Top - RacketShift,
                    _leftRacket.Margin.Right, _leftRacket.Margin.Bottom + RacketShift);
            }
        }

        public void MoveRightRacketDown()
        {
            if (_rightRacket.Margin.Top > (_rightRacket.Height - _height) / 2 - RacketShift + _border &&
                _rightRacket.Margin.Top < (_height - _rightRacket.Height) / 2 - RacketShift - _border)
            {
                _rightRacket.Margin = new Thickness(_rightRacket.Margin.Left, _rightRacket.Margin.Top + RacketShift,
                    _rightRacket.Margin.Right, _rightRacket.Margin.Bottom - RacketShift);
            }
        }

        public void MoveRightRacketUp()
        {
            if (_rightRacket.Margin.Top > (_rightRacket.Height - _height) / 2 + RacketShift + _border &&
                _rightRacket.Margin.Top < (_height - _rightRacket.Height) / 2 + RacketShift - _border)
            {
                _rightRacket.Margin = new Thickness(_rightRacket.Margin.Left, _rightRacket.Margin.Top - RacketShift,
                    _rightRacket.Margin.Right, _rightRacket.Margin.Bottom + RacketShift);
            }
        }

        public void MoveLeftRacketAsBot(double decisionAccuracy)
        {
            double distance = _leftRacket.Margin.Top - _ball.Margin.Top;

            if (_ballDirectionX < 0 && Math.Abs(distance) * 2 > _leftRacket.Width)
            {
                if (decisionAccuracy > Random.NextDouble())
                {
                    if (distance > 0)
                    {
                        MoveLeftRacketUp();
                    }
                    else
                    {
                        MoveLeftRacketDown();
                    }
                }
                else
                {
                    if (Random.Next(2) == 1)
                    {
                        if (distance > 0)
                        {
                            MoveLeftRacketDown();
                        }
                        else
                        {
                            MoveLeftRacketUp();
                        }
                    }
                }
            }
        }

        public void MoveRightRacketAsBot(double decisionAccuracy)
        {
            double distance = _rightRacket.Margin.Top - _ball.Margin.Top;

            if (_ballDirectionX > 0 && Math.Abs(distance) * 2 > _rightRacket.Width)
            {
                if (decisionAccuracy > Random.NextDouble())
                {
                    if (distance > 0)
                    {
                        MoveRightRacketUp();
                    }
                    else
                    {
                        MoveRightRacketDown();
                    }
                }
                else
                {
                    if (Random.Next(2) == 1)
                    {
                        if (distance > 0)
                        {
                            MoveRightRacketDown();
                        }
                        else
                        {
                            MoveRightRacketUp();
                        }
                    }
                }
            }
        }

        private void InitBallDirection()
        {
            _ballDirectionX = Random.Next(1, 100) * Math.Sign(Random.Next(2) - 0.5);
            _ballDirectionY = Random.Next(1, 100) * Math.Sign(Random.Next(2) - 0.5);
        }
    }
}
