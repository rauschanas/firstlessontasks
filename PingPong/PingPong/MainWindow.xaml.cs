﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace PingPong
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer _timer;
        private PingPongGame _game;

        public MainWindow()
        {
            Loaded += MainWindow_Loaded;
            Closed += MainWindow_Closed;
            InitializeComponent();
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            int border = Convert.ToInt32(UpperBorder.Height);
            int width = Convert.ToInt32(GameGrid.ActualWidth);
            int height = Convert.ToInt32(GameField.ActualHeight);

            var gameField = new PingPongGameField(LeftRacket, RightRacket, Ball, width, height, border);

            _game = new PingPongGame(gameField);

            _game.ScoreChanged += Game_ScoreChanged;

            KeyDown += MainWindow_KeyDown;
            KeyUp += MainWindow_KeyUp;

            PlayerLeftButton.Click += PlayerLeftButton_Click;
            PlayerRightButton.Click += PlayerRightButton_Click;

            _timer = new DispatcherTimer { Interval = TimeSpan.FromMilliseconds(30) };
            _timer.Tick += _game.HandleGameTick;
            _timer.Start();
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            _timer.Stop();
            _timer.Tick -= _game.HandleGameTick;

            _game.ScoreChanged -= Game_ScoreChanged;

            KeyDown -= MainWindow_KeyDown;
            KeyUp -= MainWindow_KeyUp;

            PlayerLeftButton.Click -= PlayerLeftButton_Click;
            PlayerRightButton.Click -= PlayerRightButton_Click;

            Loaded -= MainWindow_Loaded;
            Closed -= MainWindow_Closed;
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Space:
                    _game.IsPaused = !_game.IsPaused;
                    break;
                case Key.W:
                    _game.LeftRacketMoveUp = true;
                    break;
                case Key.S:
                    _game.LeftRacketMoveDown = true;
                    break;
                case Key.Up:
                    _game.RightRacketMoveUp = true;
                    break;
                case Key.Down:
                    _game.RightRacketMoveDown = true;
                    break;
            }
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.W:
                    _game.LeftRacketMoveUp = false;
                    break;
                case Key.S:
                    _game.LeftRacketMoveDown = false;
                    break;
                case Key.Up:
                    _game.RightRacketMoveUp = false;
                    break;
                case Key.Down:
                    _game.RightRacketMoveDown = false;
                    break;
            }
        }

        private void Game_ScoreChanged(object sender, ScoreChangedEventArgs e)
        {
            LeftScore.Text = e.LeftScore.ToString();
            RightScore.Text = e.RightScore.ToString();
        }

        private void PlayerLeftButton_Click(object sender, RoutedEventArgs e)
        {
            _game.IsLeftBot = !_game.IsLeftBot;
            PlayerLeftText.Text = _game.IsLeftBot ? "Bot 1" : "Player 1";
        }

        private void PlayerRightButton_Click(object sender, RoutedEventArgs e)
        {
            _game.IsRightBot = !_game.IsRightBot;
            PlayerRightText.Text = _game.IsRightBot ? "Bot 2" : "Player 2";
        }
    }
}
