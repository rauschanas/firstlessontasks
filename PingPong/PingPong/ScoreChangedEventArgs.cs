﻿using System;

namespace PingPong
{
    public class ScoreChangedEventArgs : EventArgs
    {
        public ScoreChangedEventArgs(int leftScore, int rightScore)
        {
            LeftScore = leftScore;
            RightScore = rightScore;
        }

        public int LeftScore { get; set; }
        public int RightScore { get; set; }
    }
}
