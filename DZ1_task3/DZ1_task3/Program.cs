﻿using System;

namespace DZ1_task3
{
    class Program
    {
        static void Main(string[] args)
        {
            char c = char.Parse(Console.ReadLine());
            var newChar = c + 1;
            Console.WriteLine((char)newChar);
        }
    }
}
